///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 4
///
/// @file list.hpp
/// @version 1.0
///
/// 
///
/// @author @todo Christopher Aguilar <ciaguila@hawaii.edu>
/// @brief  Lab 08a - Cat Wrangler - EE 205 - Spr 2021
/// @date   April 2021
///////////////////////////////////////////////////////////////////////////////

#pragma once

using namespace std;

#include <string>
#include "node.hpp"


class DoubleLinkedList {
protected:
   Node* head = nullptr;
   Node* tail = nullptr;

public:
  const bool empty() const;
  void push_front(Node* newNode);
  void push_back(Node* newNode);
  Node* pop_front();
  Node* pop_back();
  Node* get_first() const;
  Node* get_last() const;
  Node* get_next( const Node* currentNode ) const;
  Node* get_prev( const Node* currentNode ) const;
  unsigned int size() const;
  void swap(Node* node1, Node* node2);

   void dump() const; 
};



