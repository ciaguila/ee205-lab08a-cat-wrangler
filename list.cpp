///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// 
///
/// @file list.hpp
/// @version 1.0
///
///
///
/// @author Christopher Aguilar <ciaguila@hawaii.edu>
/// @brief  Lab 08a - Cat Wrangler - EE 205 - Spr 2021
/// @date   April 2021
///////////////////////////////////////////////////////////////////////////////

#include<iostream>

#include "list.hpp"
#include "node.hpp"

using namespace std;


const bool DoubleLinkedList::empty() const{

     // cout<< "isempty =";
      if(head == nullptr)
      {
         return true;
      }
      else
      {
         return false;
      }
   }

void DoubleLinkedList::push_front(Node* newNode){
   if(newNode == nullptr)
   {
     //cout<<"Debug: pushfront 1" << endl;
      return;
   }  
   if(head != nullptr){
      //cout<<"Debug: pushfront 2" << endl;
      newNode->next = head;
      newNode->prev = nullptr;
      head->prev = newNode;
      head = newNode;
   }
   else
   {
     // cout<<"Debug: pushfront 3"<<endl;
   newNode->next = nullptr;
   newNode->prev = nullptr;
   head = newNode;
   tail = newNode;
   }

   //cout << "pushfront" << endl;

}

Node* DoubleLinkedList::pop_front(){
   
   //cout << "popfront" << endl;

   if(head == nullptr)
   {
      return nullptr;
   }

   if(head == tail)
   {
      Node *tmp = head;
      head = nullptr;
      tail = nullptr;
      return tmp;
   }
   
   Node *tmp = head;
   head = head->next;
   head->prev = nullptr;

   tmp->next = nullptr;
   tmp->prev = nullptr;

   return tmp;

}


Node* DoubleLinkedList::pop_back(){
   //cout<<"popback"<<endl;

   if(tail == nullptr)
   {
      return nullptr;
   }

   if(head == tail)
   {
      Node *tmp = tail;
      head = nullptr;
      tail = nullptr;
      return tmp;
   }



   Node *tmp = tail;
   tail = tail->prev;

   tail->next=nullptr;

   tmp->next=nullptr;
   tmp->prev=nullptr;

   return tmp;

}



void DoubleLinkedList::push_back(Node* newNode) {
      //cout << "pushback" << endl;
      if(newNode == nullptr)
         return;

      if(tail != nullptr) {
         newNode ->next = nullptr;
         newNode->prev=tail;
         tail->next = newNode;
         tail = newNode;
      }
      else
      {
         newNode->next = nullptr;
         newNode->prev = nullptr;
         head = newNode;
         tail = newNode;
      }  
      
}

Node* DoubleLinkedList::get_first() const {
   return head;
}

Node* DoubleLinkedList::get_last() const {
   return tail;
}

void DoubleLinkedList::swap( Node* node1, Node* node2) {
     if(node1 == tail && node2 == head)
   {
      cout<<"n1 =tail, n2=head"<<endl;
      Node *tmp1 = node1;
      Node *tmp2 = node2;

      node1->next = nullptr;
      node1->next->prev = tmp2;
      node1->prev = tmp2->prev;
      node1->prev->next = tmp2;

      node2->next = tmp1->next;
      node2->next->prev = tmp1;
      node2->prev = nullptr;
      node1->prev->next = tmp1;
      delete tmp1;
      delete tmp2;
   }
   if(node1 == head && node2 == tail)
   {
      cout<<"n1 =head, n2=tail"<<endl;
      Node *tmp1 = node1;
      Node *tmp2 = node2;

      node1->next = tmp2->next;
      node1->next->prev = tmp2;
      node1->prev = nullptr;
      node1->prev->next = tmp2;

      node2->next = nullptr;
      node2->next->prev = tmp1;
      node2->prev = tmp1->prev;
      node1->prev->next = tmp1;
      delete tmp1;
      delete tmp2;
   }

   if(node1 == tail)
   {
      cout<<"n1 = tail"<<endl;
      Node *tmp1 = node1;
      Node *tmp2 = node2;

      node1->next = tmp2->next;
      node1->next->prev = tmp2;
      node1->prev = tmp2->prev;
      node1->prev->next = tmp2;

      node2->next = tmp1->next;
      node2->next->prev = tmp1;
      node2->prev = nullptr;
      node1->prev->next = tmp1;
      delete tmp1;
      delete tmp2;
   }
   if(node2 == tail)
   {
      cout<<"n2=tail"<<endl;
      Node *tmp1 = node1;
      Node *tmp2 = node2;

      node2->next = tmp1->next;
      node2->next->prev = tmp1;
      node2->prev = tmp1->prev;
      node2->prev->next = tmp1;

      node1->next = tmp2->next;
      node1->next->prev = tmp2;
      node1->prev = nullptr;
      node1->prev->next = tmp2;
      delete tmp1;
      delete tmp2;
   }
   if(node1 == head)
   {
      cout<<"n1=head"<<endl;
      Node *tmp1 = node1;
      Node *tmp2 = node2;

      node1->next = tmp2->next;
      node1->next->prev = tmp2;
      node1->prev = tmp2->prev;
      node1->prev->next = tmp2;

      node2->next = nullptr;
      node2->next->prev = tmp1;
      node2->prev = tmp1->prev;
      node1->prev->next = tmp1;
   }
   if(node2 == head)
   {
      cout<<"n2=head"<<endl;
      Node *tmp1 = node1;
      Node *tmp2 = node2;

      node2->next = tmp1->next;
      node2->next->prev = tmp1;
      node2->prev = tmp1->prev;
      node2->prev->next = tmp1;

      node1->next = nullptr;
      node1->next->prev = tmp2;
      node1->prev = tmp2->prev;
      node1->prev->next = tmp2;
   }
   if(node1 != node2)
   {
      cout<<"n1!=n2"<<endl;
      Node *tmp1 = node1;
      Node *tmp2 = node2;

      node1->next = tmp2->next;
      node1->next->prev = tmp2;
      node1->prev = tmp2->prev;
      node1->prev->next = tmp2;

      node2->next = tmp1->next;
      node2->next->prev = tmp1;
      node2->prev = tmp1->prev;
      node1->prev->next = tmp1;
   }
   if(node1 == node2)
   {
      cout<<"n1=n2"<<endl;
      return;
   }

   cout<<"nswap error"<<endl;
   
   return;
}


Node* DoubleLinkedList::get_next( const Node* currentNode ) const {
   
   return (currentNode->next);
}

Node* DoubleLinkedList::get_prev( const Node* currentNode) const {
   return (currentNode->prev);
}


unsigned int DoubleLinkedList::size() const{
  
   //cout << "getsize =";
   Node* h = head;
   unsigned int size=0;
   while(h != NULL)
   {
      //cout << "sizeadd ";
      size++;
      h = h->next;
   }
   return size;
}

void DoubleLinkedList::dump() const {
   cout << "DoubleLinkedList: head=[" << head << "]   tail=[" << tail << "]" << endl;
   for(Node* currentNode = head; currentNode != nullptr; currentNode = currentNode->next ) {
      cout << "   ";
      currentNode->dump();
   }
}
