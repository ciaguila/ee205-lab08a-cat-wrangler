///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 08a - Cat Wrangler
///
/// @file mtest.cpp
/// @version 1.0
///
/// The main loop for this lab
///
/// @author Chris Aguilar ciaguila@hawaii.edu
/// @brief  Lab 08a - Cat Wrangler - EE 205 - Spr 2021
/// @date   April 2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>

#include "cat.hpp"
#include "list.hpp"

using namespace std;

// The purpose of this mtest() function is to perform unit tests for each additional function

int main() {
	cout << "Hello World" << endl;
   
   Cat::initNames();

   Cat* newCat = Cat::makeCat();

   cout << newCat->getInfo() << endl;
   cout << "======" << endl;

   int u = 0;
   DoubleLinkedList list = DoubleLinkedList();
   u++;
   cout<< u << endl;
   
   cout << boolalpha << list.empty() << endl;
   u++;
   cout<< u << endl;

   
   for( int i = 0 ; i < 8 ; i++ ) {
      list.push_front( Cat::makeCat() );
      list.push_back( Cat::makeCat() );
   }


  cout <<  list.size() << endl;
   u++;
   cout<< u << endl;
   cout<< boolalpha <<  list.empty() << endl;
   u++;
   cout<< u << endl;

   
   cout << list.get_first() << endl;
   cout << list.get_last() <<endl;

   list.dump();

   list.swap(list.get_first(), list.get_last());

   list.dump();
}

